FROM docker.io/python:3.11.6-alpine

LABEL author="Ruslan Usmanov"
LABEL email="usmanovruslan322@gmail.com"

WORKDIR /app

ADD . /app

RUN pip install -r requirements.txt --no-cache-dir

CMD ["gunicorn", "notify.wsgi"]
