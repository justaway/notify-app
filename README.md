# Сервис уведомлений

## Описание

Данный проект является решением тестового задания для кандидатов разработчиков.

В рамках задания было необходимо разработать сервис управления рассылками API администрирования и получения статистики.

Реализованные пункты задания:
1. Созданы модели для всех сущностей.  
2. Созданы все API согласно основному заданию:  
    2.1 CRUD-ы для клиентов и рассылок  
    2.2 Для получения общей статистики по рассылкам создан эндпоинт `api/sendings/stats/`  
    2.2 Для получения статистики сообщений по конкретной рассылке создан эндпоинт `api/sendings/stats/{id}`  
3. Реализована логика создания сообщений после создания рассылок. Запуск рассылок со стартом в будущем, реализовано с помощью задач в `celery`.
4. Рализована отложенная отправка сообщений в случае ошибок.
5. Создана документация для API со Swagger UI по адресу `/docs/`. А также `/redoc/`.
6. Подготовлен docker-compose.


## Установка и запуск в режиме отладки:  
1. Склонировать репозиторий:
```bash
$ git clone git@gitlab.com:justaway/notify-app.git
```

2. Переходим в папку с проектом
```bash
$ cd notify-app
```

3. Создаем виртуальное окружение и активируем его
```bash
$ python3 -m venv venv
$ source venv/bin/activate
```

4. Устанавливаем зависимости
```bash
(venv) $ pip install -r requirements.txt
```

5. Копируем шаблон с перемеными окружения
```bash
(venv) $ cp .env.template .env
```

6. Заполняем переменные окружения
```bash
#.env
export TOKEN='Ваш токен'
export API_URL='url к внешнему сервису'

# Для генерации SECRET_KEY
# python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'
export SECRET_KEY='секрентый ключ'
export ALLOWED_HOSTS='*'
export DEBUG='True'

export DB_ENGINE='postgresql'  #  ну или sqlite3
export DB_HOST='db_host'
export DB_PORT='5432'
export DB_USER='db_user'
export DB_PASSWORD='db_password'
export DB_NAME='db_name'

export REDIS_URL='localhost'
```

7. Активируем переменные окружения
```bash
(venv) $ source .env
```

8. Применяем миграции
```bash
(venv) $ python manage.py migrate
```

9. Запускаем сервер в режиме отладки
```bash
(venv) $ python manage.py runserver localhost:8000
```

10. Запускаем celery worker в другом терминале.
```
(venv) $ celery -A notify worker -l info
```

11. Запускаем celery beat в третьем терминале
```
(venv) $ celery -A notify beat -l info
```

## Запуск с помощью docker-compose

1. Выполняем сбор static файлов
```
(venv) $ python manage.py collectstatic
```

2. Правим если надо файл `.env`.

3. Собираем контейнеры
```
$ docker-compose build
```

4. Запускаем контейнеры
```
$ docker-compose up -d
```
