# Generated by Django 4.2.7 on 2023-11-08 13:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.IntegerField()),
                ('opss_code', models.CharField(max_length=3)),
                ('tag', models.CharField(default='', max_length=50)),
                ('time_zone', models.CharField(default='UTC', max_length=35)),
            ],
        ),
        migrations.CreateModel(
            name='Sending',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.DateTimeField()),
                ('text', models.TextField(max_length=160)),
                ('filter', models.TextField()),
                ('stop_time', models.DateTimeField()),
            ],
            options={
                'ordering': ['start_time'],
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField()),
                ('status', models.CharField(choices=[('ERROR', 'Ошибка'), ('SUCCESS', 'Успешно'), ('CREATED', 'Создано')], default='CREATED', max_length=10)),
                ('id_client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.client')),
                ('id_sending', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.sending')),
            ],
            options={
                'ordering': ['created'],
            },
        ),
    ]
