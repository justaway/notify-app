from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class Sending(models.Model):
    """
    Сущность "рассылка" имеет атрибуты:
    - уникальный id рассылки
    - дата и время запуска рассылки
    - текст сообщения для доставки клиенту
    - фильтр свойств клиентов, на которых должна быть произведена рассылка
        (код мобильного оператора, тег)
    - дата и время окончания рассылки: если по каким-то причинам не успели
        разослать все сообщения - никакие сообщения клиентам после этого
        времени доставляться не должны
    """

    start_time = models.DateTimeField()
    text = models.CharField(max_length=160, null=False)  # sms max len is 160
    filter = models.CharField(max_length=200, default="")
    stop_time = models.DateTimeField()

    class Meta:
        ordering = ["start_time"]


class Client(models.Model):
    """
    Сущность "клиент" имеет атрибуты:
    - уникальный id клиента
    - номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)
    - код мобильного оператора
    - тег (произвольная метка)
    - часовой пояс
    """

    phone = models.BigIntegerField(
        validators=(
            MinValueValidator(70000000000),
            MaxValueValidator(79999999999),
        )
    )
    opss_code = models.CharField(max_length=3, editable=False)
    tag = models.CharField(max_length=50, default="")
    time_zone = models.CharField(max_length=35, default="UTC")

    def save(self, *args, **kwargs):
        self.opss_code = str(self.phone)[1:4]
        return super(Client, self).save(*args, **kwargs)


class Message(models.Model):
    """
    Сущность "сообщение" имеет атрибуты:
    - уникальный id сообщения
    - дата и время создания (отправки)
    - статус отправки
    - id рассылки, в рамках которой было отправлено сообщение
    - id клиента, которому отправили
    """

    class MessageStatus(models.TextChoices):
        ERROR = "ERROR", _("Ошибка")
        SUCCESS = "SUCCESS", _("Успешно")
        CREATED = "CREATED", _("Создано")

    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(
        max_length=10,
        choices=MessageStatus.choices,
        default=MessageStatus.CREATED,
    )
    id_sending = models.ForeignKey(
        Sending, on_delete=models.CASCADE, related_name="messages"
    )
    id_client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="messages"
    )

    class Meta:
        ordering = ["created"]
