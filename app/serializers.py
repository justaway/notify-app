from rest_framework import serializers

from app.models import Client, Message, Sending


class SendingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sending
        fields = ["id", "start_time", "text", "filter", "stop_time"]


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ["id", "phone", "opss_code", "tag", "time_zone"]


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ["id", "created", "status", "id_sending", "id_client"]
