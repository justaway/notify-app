from os import environ

import requests
from django.utils import timezone

from app.models import Client, Message, Sending


def request_send_msg(msg: Message):
    """
    Посылает POST-запрос на внешний сервис.
    msg - экземпляр класса Message.
    Если сервис отвечает успехом, то ставим статус у сообщения "успешно", иначе
    ставим статус "ошибка"
    """
    external_url = environ.get("API_URL")
    token = environ.get("TOKEN")

    json = {
        "id": msg.id,
        "phone": msg.id_client.phone,
        "text": msg.id_sending.text,
    }

    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json",
        "accept": "application/json",
    }
    try:
        responce = requests.post(
            url=external_url + str(msg.id), headers=headers, json=json
        )

        if responce.status_code != 200:
            msg.status = Message.MessageStatus.ERROR
            msg.save()
            return

        data = responce.json()
        if data["message"] == "OK":
            msg.status = Message.MessageStatus.SUCCESS
        else:
            # Сервер венул что-то не то, наверно ошибка
            msg.status = Message.MessageStatus.ERROR

    except requests.exceptions.Timeout:  # Сервер не ответил на запрос
        msg.status = Message.MessageStatus.ERROR

    except requests.exceptions.ConnectionError:  # Сервер недоступен
        msg.status = Message.MessageStatus.ERROR

    finally:
        msg.save()


def get_waiting_messages():
    """
    Получаем список сообщений, у которых "рассылка" актуальна по времени, и
    статус не равен значению "успешно"
    """
    now = timezone.now()
    return Message.objects.filter(
        id_sending__in=Sending.objects.filter(start_time__lt=now).filter(
            stop_time__gt=now
        )
    ).exclude(status=Message.MessageStatus.SUCCESS)


def get_waiting_sendings():
    """
    Получаем список актуальных рассылок, у которых еще нет сообщений
    """
    now = timezone.now()
    return (
        Sending.objects.filter(start_time__lt=now)
        .filter(stop_time__gt=now)
        .filter(messages__id=None)
    )


def create_messages_for_sending(sending: Sending):
    """
    Для данной рассылки создаем сообщения клиентам соответствующим фильтру
    """
    # Получаем список клиентов соответствующих фильтру
    clients = Client.objects.filter(
        tag__in=sending.filter.split()
    ) | Client.objects.filter(opss_code__in=sending.filter.split())

    # Если клиентов нет, то ничего не делаем
    if not clients:
        return

    for client in clients:
        message = Message(
            created=timezone.now(), id_sending=sending, id_client=client
        )
        message.save()
