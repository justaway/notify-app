from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from app.models import Message, Sending
from app.service import create_messages_for_sending, request_send_msg


@receiver(post_save, sender=Sending)
def on_sendings_update(sender, instance: Sending, created: bool, **kwargs):
    """
    Функция срабаотывает после создания/обновления расслыки
    """
    # Если создали новую рассылку
    if created:
        # Если текущее время меньше времени начала или больше времени окончания
        # то ничего не делаем
        now_utc = timezone.now()
        if now_utc > instance.stop_time or now_utc < instance.start_time:
            return

        create_messages_for_sending(instance)
    # Если произошло обновление
    else:
        pass  # TODO: А что делать-то если рассылка была обновлена?


@receiver(post_save, sender=Message)
def on_create_message(sender, instance: Message, created: bool, **kwargs):
    """
    Срабатывает после создания нового сообщения, пытается отправить сообщение
    """
    if created:
        request_send_msg(instance)
