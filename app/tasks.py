from app.service import (
    create_messages_for_sending,
    get_waiting_messages,
    get_waiting_sendings,
    request_send_msg,
)
from notify.celery import app


@app.task
def actuate_messages():
    """
    Получаем список актуальных неуспешных сообщений и выполянем по ним запрос
    на внешний сервис
    """
    for msg in get_waiting_messages():
        request_send_msg(msg)


@app.task
def actuate_sendings():
    """
    Получаем список актуальных рассылок, без сообщений. Затем создаем
    для них сообщения
    """
    for sending in get_waiting_sendings():
        create_messages_for_sending(sending)
