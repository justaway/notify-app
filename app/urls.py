from django.urls import path

from app import views

urlpatterns = [
    path("sendings/", views.SendingList.as_view()),
    path("sendings/<int:pk>", views.SendingDetail.as_view()),
    path("clients/", views.ClientList.as_view()),
    path("clients/<int:pk>", views.ClientDetail.as_view()),
    path("messages/", views.MessageList.as_view()),
    path("sendings/stats/", views.total_stats),
    path("sendings/stats/<int:pk>", views.stats_by_sending),
]
