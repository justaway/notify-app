from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response

from app.models import Client, Message, Sending
from app.serializers import (
    ClientSerializer,
    MessageSerializer,
    SendingSerializer,
)


class SendingList(generics.ListCreateAPIView):
    queryset = Sending.objects.all()
    serializer_class = SendingSerializer


class SendingDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Sending.objects.all()
    serializer_class = SendingSerializer


class ClientList(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageList(generics.ListAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


@api_view(["GET"])
def total_stats(requsest):
    sendings = Sending.objects.all()
    total_sendgins = sendings.count()
    total_messages = Message.objects.count()

    response = {
        "total number of sendings": total_sendgins,
        "total number of messages": total_messages,
    }

    num_msg = {}

    for s in sendings:
        stats = {}
        messages = Message.objects.filter(id_sending=s).all()
        total = messages.count()
        success = messages.filter(status=Message.MessageStatus.SUCCESS).count()
        error = messages.filter(status=Message.MessageStatus.ERROR).count()
        stats["id_sending"] = s.id
        stats["total messages"] = total
        stats["success messages"] = success
        stats["error messages"] = error
        num_msg[s.id] = stats

    response["messages by sendings"] = num_msg
    return Response(response)


@api_view(["GET"])
def stats_by_sending(requsest, pk: int):
    """
    Возвращает статистику сообщений по рассылкам, с группировкой по статусам
    """
    sending = get_object_or_404(Sending, pk=pk)

    messages = Message.objects.filter(id_sending=sending).all()
    serializer = MessageSerializer(messages, many=True)

    return Response(serializer.data)
